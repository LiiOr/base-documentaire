<?php 

include('header.php'); ?>

<main class="container">

    <h2 class="text-center fw-bold py-5">Ajouter un document</h2>

    <a class="btnSecond" href="index.php?action=mesDocs"><i class="fas fa-chevron-left mx-3"></i>Revenir aux documents</a>

    <form action="index.php?action=sendDocument" method="POST" class="my-5" enctype="multipart/form-data">

        <div id="divVignettes"> </div>

        <div class="mb-3">
            <label for="formFile" class="form-label">Veuillez sélectionner le dossier à envoyer</label>
            <!-- <input type="hidden" name="MAX_FILE_SIZE" value="2097152" /> -->
            <input class="form-control" type="file" name="formFile" id="formFile" accept=".zip" required>
        </div>

        <button class="btnMain btnValidation" type="submit">Enregistrer le document</button>

    </form>

</main>

<?php include('footer.php'); ?>