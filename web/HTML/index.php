<?php

session_start();

$url = "http://" . $_SERVER['SERVER_NAME'] . ":8067/";
if($_SERVER['SERVER_NAME'] === "localhost") $url = "http://infodocs_ws/";


/* -------------------------------------------------------------------------- */
/*                                   ROUTER                                   */
/* -------------------------------------------------------------------------- */

$action = $_GET['action'];

switch ($action) {
    case 'search':
        search($param);
        break;
    case 'mesDocs':
        mesDocs();
        break;
    case 'login':
        login();
        break;
    case 'logout':
        logout();
        break;
    case 'sendDocument':
        sendDocument();
        break;
    case 'update_document':
        update_document($param);
        break;
}

/* -------------------------------------------------------------------------- */
/*                                  FONCTIONS                                 */
/* -------------------------------------------------------------------------- */

function search()
{
    global $url;

    if (isset($_POST['param'])) {
        $param = str_replace(" ", "", $_POST['param']);
    } else {
        $param = false;
    }

    //récupération des slugs filtrés
    $slugsShearch = [];
    foreach($_POST as $index => $paramSlugs) {
        if($index !== 'param' && $index !== 'arborescence' && $index !== 'niveauArbo') {
            $slugsShearch[] = $paramSlugs;
        }
    }


    //Récupération de liste de documents
    if ($param) {
        $documents = file_get_contents($url . "find/" . $param);
    } else {
        $documents = file_get_contents($url . "documents");
    }
    $documents = json_decode($documents);

    //récupération des tags
    $tags = file_get_contents($url . "slugs");
    $tags = json_decode($tags);
    $tags = $tags->tags;

    $tabTag = [];

    foreach ($tags as $tag) {
        $tabTag[$tag->idtags] = $tag->libele;
    }

    //mise a jour du fichier de chemins
    updatePath();

    require_once('search.php');
}

function filtreSlugs ($tabOrigin, $tabSlugs) 
{
    global $url;


    $slugs = file_get_contents($url . "slugs");
    
}

function filtreArbo ($tabOrigin, $chemin, $niveau) 
{
    global $url;


    $routes = file_get_contents($url ."paths");
    $routes = json_decode($routes);
    $routes = $routes->paths;
    $newRoutes = [];
    foreach($routes as $route) {
        if($route->chemin[0] == '/') {
            $newRoutes[] = substr($route->chemin, 1);
        } else {
            $newRoutes[] = $route->chemin;
        }
    }
    
}


function mesDocs($param = false)
{
    global $url;

    $documents = file_get_contents($url . "documents"); //changer le nom de la méthode API
    //$documents = file_get_contents($url ."document_company/". $param); //changer le nom de la méthode API
    $documents = json_decode($documents);

    require_once('document_list.php');
}

function sendDocument($origin = NULL)
{
    if (isset($_FILES['formFile'])) {

        /* Gestion des cas d'erreurs */
        if ($_FILES['formFile']['error']) {
            switch ($_FILES['formFile']['error']) {
                case 1: //UPLOAD_ERR_INI_SIZE
                    $message = "le fichier dépasse la taille autorisée par la directive serveur php.ini.";
                    break;
                case 2: //UPLOAD_ERR_FORM_SIZE
                    $message = "le fichier dépasse la taille autorisée par le formulaire.";
                    break;
                case 3: //UPLOAD_ERR_PARTIAL:
                    $message = "le fichier n'a été que partiellement téléchargé.";
                    break;
                case 4: //UPLOAD_ERR_NO_FILE:
                    $message = "aucun fichier trouvé.";
                    break;
                case 6: //UPLOAD_ERR_NO_TMP_DIR:
                    $message = "dossier temporaire inexistant.";
                    break;
                case 7: //UPLOAD_ERR_CANT_WRITE:
                    $message = "impossible d'écrire le fichier sur le disque.";
                    break;
                case 8: //UPLOAD_ERR_EXTENSION:
                    $message = "erreur d'extension.";
                    break;
                default:
                    $message = "erreur inconnue. Veuillez poster un fichier .zip";
                    break;
            }
            $_SESSION['notif'] = '<div class="alert alert-danger" role="alert">Une erreur est survenue lors de l\'envoi du fichier : ' . $message . '</div>';
            header('Location: document_form.php'); // si un code ERR est trouvé, on redirige vers l'accueil avec notification erreur
        } else {

            $documentTmp = explode('/',$_FILES['formFile']['tmp_name'])[2];
            $document = $_FILES['formFile']['name'];
            exec('rm -r /var/www/html/uploads/' . explode('.', $document)[0]);

            try { // Envoi du document vers le dossier uploadedFiles
                $res = move_uploaded_file($_FILES['formFile']['tmp_name'], '/var/www/html/uploads/' . $document);
                exec('unzip /var/www/html/uploads/' . $document . ' -d /var/www/html/uploads/');
                exec('rm /var/www/html/uploads/' . $document . '.zip');
                $_SESSION['notif'] = '<div class="alert alert-success" role="alert">Le fichier a bien été envoyé</div>';
            } catch (Exception $e) {
                error_log('Impossible de déplacer le fichier transféré : ' . $e->getMessage());
            }

            if ($res) {
                global $url;
                $string = file_get_contents("/var/www/html/uploads/" . explode('.', $document)[0] . "/JSON.json");
                $json = json_decode($string, true);
                //TODO foreach slug get if_exist, get id if exist else insert it and get id
                $lesTags = $json['tags'][0];
                foreach($json['tags'] as $tag){
                    if($tag !== $json['tags'][0]){
                        $lesTags = $lesTags . ';' . $tag;
                    }
                }

                $curl = curl_init();
                $postdata = http_build_query(
                    array(
                        'nom' => $json['nom'],
                        'idproprietaire' => $_SESSION['identreprise'],
                        'listeslug' => $lesTags,
                        'chemin' => $json['chemin'],
                        'visibilite' => $json['visibilite'],
                        'size' => $_FILES['formFile']['size']
                    )
                );
                $options = array(
                    CURLOPT_URL => $url . "documents",
                    CURLOPT_HEADER => false,
                    CURLOPT_POST => 1, 
                    CURLOPT_POSTFIELDS => $postdata,
                    CURLOPT_RETURNTRANSFER => true
                );
                curl_setopt_array($curl, $options);
                $result = curl_exec($curl);
                curl_close($curl);
            }
            if (!$res || !$result) {
                $_SESSION['notif'] = '<div class="alert alert-danger" role="alert">Une erreur est survenue lors de l\'envoi du fichier</div>';
            }

            if(!$origin){
                header('Location: index.php?action=mesDocs'); // redirection vers liste de documents
            }{
                header('Location: index.php?action=search'); // redirection vers liste de documents
            }
        }
    }
}

function update_document(){
    $idDoc = $_GET['param'];
    global $url;

    //suppression de l'ancien document 
    file_get_contents($url . "documents/delete/". $idDoc);
    
    sendDocument();
}



/* -------------------------------------------------------------------------- */
/*                                Log IN / OUT                                */
/* -------------------------------------------------------------------------- */

function login()
{
    global $url;

    $user = file_get_contents($url . "login");
    if ($user !== null) {
        //$_SESSION['username'] = $user['mail'];
        //$_SESSION['idrole'] = $user['idrole'];
        header('Location: home.php');
    } else {
        echo 'Vous n\'avez pas les droits d\'accès.';
    }
    //

}


function logout()
{
    session_start();

    // Détruit la session.
    if (session_destroy()) {
        // Redirige vers la page d'accueil
        header("Location: home.php");
    }
}



/* -------------------------------------------------------------------------- */
/*                                Arborescence                                */
/* -------------------------------------------------------------------------- */

function addPath($route)
{
    $routes = json_decode(file_get_contents('../autres/routes.json'));
    if (array_search($route, $routes) === false) {
        $routes[] = $route;
        $old = fopen("routes.json", "w");
        fwrite($old, json_encode($routes, JSON_PRETTY_PRINT));
    } else {
        echo 'Route déjà existante';
    }
}

function deletePath($route)
{
    $routes = json_decode(file_get_contents('../autres/routes.json'));
    $indiceRoute = array_search($route, $routes);
    if ($indiceRoute === false) {
        echo 'Route non existante';
    } else {
        unset($routes[$indiceRoute]);
        $old = fopen("routes.json", "w");
        fwrite($old, json_encode($routes, JSON_PRETTY_PRINT));
    }
}

function updatePath()
{
    global $url;

    $routes = file_get_contents($url ."paths");
    $routes = json_decode($routes);
    $routes = $routes->paths;
    $newRoutes = [];
    foreach($routes as $route) {
        if($route->chemin[0] == '/') {
            $newRoutes[] = substr($route->chemin, 1);
        } else {
            $newRoutes[] = $route->chemin;
        }
    }
    $old = fopen("../autres/routes.json", "w");
    fwrite($old, json_encode($newRoutes, JSON_PRETTY_PRINT));
}

