<?php include('header.php'); ?>

<main class="container">

  <h2 class="text-center fw-bold py-5">Vos documents</h2>

  <a class="btnSecond" href="document_form.php"><i class="fas fa-plus-square mx-2"></i>Ajouter un document</a>

  <table class="table table-hover mt-3">
    <tbody>

      <?php foreach ($documents as $doc) {  
        
        $doc->size = $doc->size / 1000; ?>

        <tr> 
          <th scope="row"><?php echo $doc->nom; ?></th>
          <td><?php echo $doc->created_at; ?></td>
          <td><?php echo $doc->size ?> Ko</td>
          <td><a href="<?php echo $doc->nom; ?>"><i class="fas fa-cloud-download-alt fs-3 redIcon"></i></a></td>
          <td><a href=""><i class="fas fa-edit fs-3 redIcon"></i></a></td>
        </tr>
      <?php } ?>

    </tbody>
  </table>

  <button class="btnSecond" href="document_form.php"><i class="fas fa-plus-square mx-2"></i>Ajouter un document</button>

</main>


<?php include('footer.php'); ?>