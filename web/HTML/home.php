<?php include('header.php');?>

<main id="home"> 

    <div class="position-absolute top-50 start-50 translate-middle">

        <h2 class="text-center fw-bold fs-1">Rechercher un document</h2>

        <form action="index.php?action=search" method="POST">
            <div class="input-group input-group-lg mb-5">
                <input type="text" class="form-control" placeholder="Ex: Notice Imprimante Epson Home 3300" name="param">
                <button class="btn btn-outline-dark" type="submit" id="button-search">Ok</button>
            </div>
        </form>

        <div class="input-group input-group-lg">
            <a class="btn btn-dark btn-lg fs-3" href="index.php?action=search" type="button" id="button-addon2">Voir tout les documents<i class="fas fa-chevron-right text-light ms-3"></i></a>
        </div>

    </div>

</main>



<?php include('footer.php'); ?>