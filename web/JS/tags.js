//node object to tab of text
function nodelisteToArrayText (monObjet) {
    if(monObjet !== undefined)  {
        return Object.keys(monObjet).map(function(cle) {
            return monObjet[cle].textContent;
        });
    } else {
        return false;
    }
}

let searchTag = document.querySelector('.searchtags');
searchTag.addEventListener('focusin', (event) => {
    // if(searchTag.fi) {
        document.querySelector('#tags').style.display = 'block';
    // }
});

document.querySelector('.searchtags').addEventListener('focusout', (event) => {
    window.setTimeout(() => {document.querySelector('#tags').style.display = 'none';}, 200);
});

//gestion de la recherche des tags
document.querySelector('.searchtags').addEventListener('keyup', (event) => {
    let regex = RegExp('^'+event.target.value.toLowerCase(), 'm');
    let finded = false;
    document.querySelectorAll('#tags p').forEach((element) => {
        text = element.textContent.toLowerCase();
        if(text.search(regex) !== -1) {
            element.style.display = 'block';
            finded = true
        } else {
            element.style.display = 'none';
        }
    });
    if (finded === false) {
        document.querySelector('#tags').style.display = 'none';
    } else {
        document.querySelector('#tags').style.display = 'block';
    }
});

//ajout des tags au clic sur ceux ci
document.querySelectorAll('#tags p, .tagsRecommended button').forEach((tagP)=> {
    tagP.addEventListener('click', (event) => {
        event.preventDefault();
        //il faut ajouter l'ajout dans la recherche le tag
        if (nodelisteToArrayText(document.querySelectorAll('.research button')).indexOf(tagP.textContent) === -1) {
            var newButton = document.createElement("button");
            var newContent = document.createTextNode(tagP.textContent);
            newButton.appendChild(newContent);
            var newIcone = document.createElement("i");
            newIcone.classList.add('ms-2', 'wIcon', 'fas', 'fa-times')
            //suppression du tag en cliquant sur la crois
            newIcone.addEventListener('click', (event) => {
                //il faut ajouter la suppression dans la recherche le tag
                event.preventDefault();
                newButton.remove();
            });
            newButton.appendChild(newIcone);
            document.querySelector('.research').appendChild(newButton);

            fetch('flowers.jpg')
            .then(function(response) {
                return response.blob();
            })
            .then(function(myBlob) {
                const objectURL = URL.createObjectURL(myBlob);
                myImage.src = objectURL;
            });
        }
    });
});