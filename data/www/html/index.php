<?php
	// header('Access-Control-Allow-Origin:*');

	use Psr\Http\Message\ResponseInterface as Response;
	use Psr\Http\Message\ServerRequestInterface as Request;
	use Slim\Factory\AppFactory;
	require __DIR__ . '/../slim/vendor/autoload.php';

	// https://www.slimframework.com/docs/v4/cookbook/enable-cors.html
	// https://www.html5rocks.com/static/images/cors_server_flowchart.png
	// ***	
	function buildResponse($resp, $ret) {
		$newResponse = $resp->withHeader('Content-type', 'application/json')
				->withHeader('Access-Control-Allow-Origin', '*')
				->withHeader('Access-Control-Allow-Credentials', 'true')
				->withHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Origin, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers')
				->withHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,PATCH,OPTIONS');
		if ($ret != null) {
			$newResponse->getBody()->write(json_encode($ret));
		}
		return $newResponse;
	}

	$app = AppFactory::create();
	
	require __DIR__ . '/api.php';
?>
