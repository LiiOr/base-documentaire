<?php

function initDatabase(): ?PDO
{
	try {
		$host     = 'baptiste.lpweb-lannion.fr';
		$username = 'info_doc';
		$password = 'gmB/^F7H>(7vc;jl';
		$name     = 'info_doc';
		$port	  =  3306;
		$cnx = new PDO("mysql:host=$host;port=$port;dbname=$name;charset=utf8", $username, $password);
		$cnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $cnx;
	} catch (PDOException $e) {
		echo '[PDO error]: ' . $e->getMessage() . '<br/>';
		return false;
	}
}

function isLogged()
{
	session_start();
	return ($_SESSION['logged'] === true);
}

$app->get('/', function ($req, $resp) {
	return buildResponse($resp, 'Ca maaaaarche !');
});

$app->get('/login', function ($req, $resp) {

	/*if (isset($_POST['Email']) && isset($_POST['Password'])) {
			$mail = htmlspecialchars($_POST['Email']);
			$pwd = htmlspecialchars($_POST['Password']);
			var_dump($mail);
			
			session_start();
			$pdo = initDatabase();

			$user = $pdo->query("SELECT * FROM _user WHERE email='$mail' and motdepasse='$pwd'")->fetch();
			var_dump('requete user passée');
			
			//print('Hello ' . $resCheckUser->username);
			return buildResponse($resp, $user);
		}*/
	session_start();
	$_SESSION['logged'] = true;		// Facile !!! :-)
	return buildResponse($resp, 'Logged!');
});

$app->get('/logout', function ($req, $resp) {
	session_start();
	//unset($_SESSION['logged']);
	session_destroy();
	return buildResponse($resp, 'Logout !');
});

$app->get('/raz', function ($req, $resp) {
	if (!isLogged()) {
		return $resp->withStatus(401);   // Unauthorized
	}
	@unlink(SAMPLE_DATA);	// Suppression du fichier data
	return buildResponse($resp, 'DB RàZ !');
});

// $app->get('/logged', function($req, $resp) {
// 	return buildResponse($resp, 'Logged ? ' . (isLogged() ? 'Y' : 'N'));
// })

/* -------------------------------------------------------------------------- */
/*                       Les API get pour les documents                       */
/* -------------------------------------------------------------------------- */
$app->get('/documents', function ($req, $resp) {
	$pdo = initDatabase();
	$ret = array();
	$documents = [];
	$stmt = $pdo->query('SELECT * FROM `_document` where `visibilite` = 1 ORDER BY modifiedat DESC LIMIT 10');

	$ret = $stmt->fetchAll(PDO::FETCH_ASSOC);


	return buildResponse($resp, $ret);
});

$app->get('/documents/offset/{off}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	$ret = array();
	$off = $args['off'];
	$documents = [];
	$stmt = $pdo->query("SELECT * FROM `_document` where `visibilite` = 1 ORDER BY modifiedat DESC LIMIT 10 OFFSET $off");

	$ret = $stmt->fetchAll(PDO::FETCH_ASSOC);


	return buildResponse($resp, $ret);
});

/* ---------------------- fichiers suite a la recherche --------------------- */
$app->get('/find/{nom}', function ($req, $resp, $args) {

	$nom = $args['nom'];
	$dbh = initDatabase();

	$stmt = $dbh->prepare("SELECT * FROM _document WHERE nom LIKE '%$nom%' LIMIT 10");
	$stmt->execute();
	$ret = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return buildResponse($resp, $ret);
});

$app->get('/find/{nom}/{offset}', function ($req, $resp, $args) {

	$nom = $args['nom'];
	$off = $args['offset'];
	$dbh = initDatabase();

	$stmt = $dbh->prepare("SELECT * FROM _document WHERE nom LIKE '%$nom%' LIMIT 10 OFFSET $off");
	$stmt->execute();
	$ret = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return buildResponse($resp, $ret);
});

$app->get('/documents/{id}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	$ret = array();
	$id = $args['id'];
	$documents = [];

	$stmt = $pdo->prepare('SELECT * FROM `_document` WHERE `iddocument` = :id');
	$stmt->execute(['id' => $id]);
	while ($row = $stmt->fetchObject()) {
		$documents[] = [
			'iddoc' => (int)$row->iddocument,
			'idproprio' => (int)$row->idproprietaire,
			'nom' => (string)$row->nom,
			'slugs' => (string)$row->listeslug,
			'created_at' => (string)$row->createdat,
			'modified_at' => (string)$row->modifiedat,
			'chemin' => (string)$row->chemin,
			'public' => (string)$row->visibilite,
			'size' => (int)$row->size,
		];
	}

	$ret = array(
		'documents' => (array)$documents,
	);

	return buildResponse($resp, $ret);
});

$app->get('/documents_tags/{tags}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	$ret = array();
	$tag = $args['tags'];
	$documents = [];

	$stmt = $pdo->prepare("SELECT * FROM _document WHERE (listeslug like ? or listeslug like ? or listeslug like ? or listeslug like ?)");
	$stmt->execute([$tag . ':%', '%:' . $tag . ':%', $tag, '%:' . $tag]);

	while ($row = $stmt->fetchObject()) {
		$documents[] = [
			'iddoc' => (int)$row->iddocument,
			'idproprio' => (int)$row->idproprietaire,
			'nom' => (string)$row->nom,
			'slugs' => (string)$row->listeslug,
			'created_at' => (string)$row->createdat,
			'modified_at' => (string)$row->modifiedat,
			'chemin' => (string)$row->chemin,
			'public' => (string)$row->visibilite,
			'size' => (int)$row->size,
		];
	}

	$ret = array(
		'documents' => (array)$documents,
	);

	return buildResponse($resp, $ret);
});

/* -------------------------------------------------------------------------- */
/*           id_entreprise récupéré par la session de l'utilisateur,          */
/*               pour récupérer les docs partager à l'entreprise              */
/* -------------------------------------------------------------------------- */
$app->get('/documents_company/{id_entreprise}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	$ret = array();
	$id = $args['id_entreprise'];
	$documents = [];

	$stmt = $pdo->prepare('SELECT * FROM `_document` WHERE `idproprietaire` = :id');
	$stmt->execute(['id' => $id]);
	/*while ($row = $stmt->fetchObject()) {
			$documents[] = [
				'iddoc' => (int)$row->iddocument,
                'idproprio' => (int)$row->idproprietaire,
				'nom' => (string)$row->nom,
                'slugs' => (string)$row->listeslug,
                'created_at' => (string)$row->createdat,
                'modified_at' => (string)$row->modifiedat,
                'chemin' => (string)$row->chemin,
				'public' => (string)$row->visibilite,
				'size' => (int)$row->size,
			];
		}
		
		$ret = array(
			'documents' => (array)$documents,
		);*/
	$ret = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return buildResponse($resp, $ret);
});

/* -------------------------------------------------------------------------- */
/*                         Les API get pour les users                         */
/* -------------------------------------------------------------------------- */
$app->get('/users', function ($req, $resp) {
	$pdo = initDatabase();
	$ret = array();
	$users = [];
	$stmt = $pdo->query('SELECT * FROM `_user`');
	while ($row = $stmt->fetchObject()) {
		$users[] = [
			'iduser' => (int)$row->iduser,
			'idrole' => (int)$row->idrole,
			'identreprise' => (int)$row->identreprise,
			'nom' => (string)$row->nom,
			'prenom' => (string)$row->prenom,
			'email' => (string)$row->email,
			'mdp' => (string)$row->motdepasse,
		];
	}

	$ret = array(
		'users' => (array)$users,
	);

	return buildResponse($resp, $ret);
});

$app->get('/users_company/{company}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	$ret = array();
	$users = [];
	$id = $args['company'];
	$stmt = $pdo->prepare('SELECT * FROM `_user` WHERE identreprise = :id');
	$stmt->execute(['id' => $id]);
	while ($row = $stmt->fetchObject()) {
		$users[] = [
			'iduser' => (int)$row->iduser,
			'idrole' => (int)$row->idrole,
			'identreprise' => (int)$row->identreprise,
			'nom' => (string)$row->nom,
			'prenom' => (string)$row->prenom,
			'email' => (string)$row->email,
			'mdp' => (string)$row->motdepasse,
		];
	}

	$ret = array(
		'users' => (array)$users,
	);

	return buildResponse($resp, $ret);
});

$app->get('/users/{id}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	$ret = array();
	$users = [];
	$id = $args['id'];
	$stmt = $pdo->prepare('SELECT * FROM `_user` WHERE iduser = :id');
	$stmt->execute(['id' => $id]);
	while ($row = $stmt->fetchObject()) {
		$users[] = [
			'iduser' => (int)$row->iduser,
			'idrole' => (int)$row->idrole,
			'identreprise' => (int)$row->identreprise,
			'nom' => (string)$row->nom,
			'prenom' => (string)$row->prenom,
			'email' => (string)$row->email,
			'mdp' => (string)$row->motdepasse,
		];
	}

	$ret = array(
		'users' => (array)$users,
	);
	// Prévoir condition pour ne pas afficher users en clair
	return buildResponse($resp, $ret);
});


/* -------------------------------------------------------------------------- */
/*                     Les API get pour les entreprises                     */
/* -------------------------------------------------------------------------- */
$app->get('/company', function ($req, $resp) {
	$pdo = initDatabase();
	$ret = array();
	$entreprises = [];
	$stmt = $pdo->query('SELECT * FROM `_entreprise`');
	while ($row = $stmt->fetchObject()) {
		$entreprises[] = [
			'identreprise' => (int)$row->identreprise,
			'libele' => (string)$row->libele,
		];
	}

	$ret = array(
		'entreprises' => (array)$entreprises,
	);

	return buildResponse($resp, $ret);
});

$app->get('/company/{id}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	$ret = array();
	$entreprises = [];
	$id = $args['id'];
	$stmt = $pdo->prepare('SELECT * FROM `_entreprise` WHERE identreprise = :id');
	$stmt->execute(['id' => $id]);
	while ($row = $stmt->fetchObject()) {
		$entreprises[] = [
			'identreprise' => (int)$row->identreprise,
			'libele' => (string)$row->libele,
		];
	}

	$ret = array(
		'entreprises' => (array)$entreprises,
	);

	return buildResponse($resp, $ret);
});

/* -------------------------------------------------------------------------- */
/*                          Les API get pour les tags                         */
/* -------------------------------------------------------------------------- */
$app->get('/slugs', function ($req, $resp) {
	$pdo = initDatabase();
	$ret = array();
	$tags = [];
	$stmt = $pdo->query('SELECT * FROM `_tags`');
	while ($row = $stmt->fetchObject()) {
		$tags[] = [
			'idtags' => (int)$row->idtags,
			'libele' => (string)$row->libele,
		];
	}

	$ret = array(
		'tags' => (array)$tags,
	);

	return buildResponse($resp, $ret);
});

$app->get('/slugs/{id}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	$ret = array();
	$tags = [];
	$id = $args['id'];
	$stmt = $pdo->prepare('SELECT * FROM `_tags` WHERE idtags = :id');
	$stmt->execute(['id' => $id]);
	while ($row = $stmt->fetchObject()) {
		$tags[] = [
			'idtags' => (int)$row->idtags,
			'libele' => (string)$row->libele,
		];
	}

	$ret = array(
		'tags' => (array)$tags,
	);

	return buildResponse($resp, $ret);
});


/* -------------------------------------------------------------------------- */
/*                          Les API get pour les paths                        */
/* -------------------------------------------------------------------------- */
$app->get('/paths', function ($req, $resp) {
	$pdo = initDatabase();
	$ret = array();
	$tags = [];
	$stmt = $pdo->query('SELECT iddocument, chemin FROM `_document`');
	while ($row = $stmt->fetchObject()) {
		$paths[] = [
			'iddocument' => (int)$row->iddocument,
			'chemin' => (string)$row->chemin,
		];
	}

	$ret = array(
		'paths' => (array)$paths,
	);

	return buildResponse($resp, $ret);
});

/*
		Le POST va retourner comme réponse la ressource nouvellement créée
	*/
/* -------------------------------------------------------------------------- */
/*                         API Post pour les documents                        */
/* -------------------------------------------------------------------------- */
$app->post('/documents', function ($req, $resp, $args) {
	$pdo = initDatabase();
	// if (!isLogged()) {
	// 	return $resp->withStatus(401);   // Unauthorized
	// }
	echo ('entrée dans api');

	$params = $req->getParsedBody();
	$nom = $params['nom'];
	$proprio = $params['idproprietaire'];
	$slugs = $params['listeslug'];
	$chemin = $params['chemin'];
	$public = $params['visibilite'];
	$size = $params['size'];
	var_dump($params);

	if ($nom == '') {
		return $resp->withStatus(400);   // Bad request
	}
	try {
		//$stmt = $pdo->prepare('INSER INTO `_document` (`idproprietaire`, `nom`, `listeslug`, `chemin`, `visibilite`, `size`) select :proprio, :nom, :slugs, :chemin, :public, :size WHERE NOT EXISTS (SELECT * FROM `_document` WHERE `chemin` = :chemin AND `nom` = :nom); ');
		$stmt = $pdo->prepare('INSERT INTO `_document` (`idproprietaire`, `nom`, `listeslug`, `chemin`, `visibilite`, `size`) VALUES (:proprio, :nom, :slugs, :chemin, :public, :size);');	
		$stmt->execute(['proprio' => $proprio, 'nom' => $nom, 'slugs' => $slugs, 'chemin' => $chemin, 'public' => $public, 'size' => $size]);
	} catch (Exception $e) {
		echo 'Echec envoi du document : ' . $e;
	}
	return $resp->withStatus(200);
});


/* -------------------------------------------------------------------------- */
/*                         API Post pour les users                        */
/* -------------------------------------------------------------------------- */
$app->post('/users', function ($req, $resp, $args) {
	$pdo = initDatabase();
	// if (!isLogged()) {
	// 	return $resp->withStatus(401);   // Unauthorized
	// }

	$params = $req->getParsedBody();
	$nom = $params['nom'];
	$prenom = $params['prenom'];
	$role = $params['idrole'];
	$entreprise = $params['identreprise'];
	$email = $params['email'];
	$mdp = $params['motdepasse'];
	if ($nom == '') {
		return $resp->withStatus(400);   // Bad request
	}

	$stmt = $pdo->prepare('SELECT * FROM `_user` WHERE email = :email');
	$stmt->execute(['email' => $email]);

	if ($stmt->rowCount() == 0) {

		$stmt = $pdo->prepare('INSERT INTO `_user` (`idrole`, `identreprise`, `nom`, `prenom`, `email`, `motdepasse`) VALUES (:idrole, :identreprise, :nom, :prenom, :email, :motdepasse)');
		$stmt->execute(['idrole' => $role, 'identreprise' => $entreprise, 'nom' => $nom, 'prenom' => $prenom, 'email' => $email, 'motdepasse' => $mdp]);

		return $resp->withStatus(200);
	} else {
		return $resp->withStatus(404);
	}
});

/* -------------------------------------------------------------------------- */
/*                         API Post pour les entreprises                      */
/* -------------------------------------------------------------------------- */
$app->post('/company', function ($req, $resp, $args) {
	$pdo = initDatabase();
	// if (!isLogged()) {
	// 	return $resp->withStatus(401);   // Unauthorized
	// }

	$params = $req->getParsedBody();
	$nom = $params['libele'];
	if ($nom == '') {
		return $resp->withStatus(400);   // Bad request
	}

	$stmt = $pdo->prepare('SELECT * FROM `_entreprise` WHERE libele = :nom');
	$stmt->execute(['nom' => $nom]);

	if ($stmt->rowCount() == 0) {

		$stmt = $pdo->prepare('INSERT INTO `_entreprise` (`libele`) VALUES (:nom)');
		$stmt->execute(['nom' => $nom]);

		return $resp->withStatus(200);
	} else {
		return $resp->withStatus(404);
	}
});

/* -------------------------------------------------------------------------- */
/*                         API Post pour les tags                             */
/* -------------------------------------------------------------------------- */
$app->post('/tags', function ($req, $resp, $args) {
	$pdo = initDatabase();
	// if (!isLogged()) {
	// 	return $resp->withStatus(401);   // Unauthorized
	// }

	$params = $req->getParsedBody();
	$nom = $params['libele'];
	if ($nom == '') {
		return $resp->withStatus(400);   // Bad request
	}

	$stmt = $pdo->prepare('SELECT * FROM `_tags` WHERE libele = :nom');
	$stmt->execute(['nom' => $nom]);

	if ($stmt->rowCount() == 0) {

		$stmt = $pdo->prepare('INSERT INTO `_tags` (`libele`) VALUES (:nom)');
		$stmt->execute(['nom' => $nom]);

		return $resp->withStatus(200);
	} else {
		return $resp->withStatus(404);
	}
});



/*
		Le PUT va simplement retourner une réponse sans BODY, qui indiquera que la modif est OK
	*/
/* -------------------------------------------------------------------------- */
/*                         API Put pour les documents                         */
/* -------------------------------------------------------------------------- */
$app->put('/documents/{id}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	// if (!isLogged()) {
	// 	return $resp->withStatus(401);   // Unauthorized
	// }
	$id = $args['id'];

	$params = $req->getParsedBody();
	$nom = $params['nom'];
	$proprio = $params['idproprietaire'];
	$slugs = $params['listeslug'];
	$chemin = $params['chemin'];
	$public = $params['visibilite'];
	$size = $params['size'];
	if ($nom == '') {
		return $resp->withStatus(400);   // Bad request
	}
	$stmt = $pdo->prepare('UPDATE `_document` SET `idproprietaire` = :proprio, `nom` = :nom, `listeslug` = :slugs, `chemin` = :chemin, `visibilite` = :public, `size` = :size WHERE `iddocument` = :id');
	$stmt->execute(['id' => $id, 'proprio' => $proprio, 'nom' => $nom, 'slugs' => $slugs, 'chemin' => $chemin, 'public' => $public, 'size' => $size]);

	return $resp->withStatus(200);
});

/* -------------------------------------------------------------------------- */
/*                         API Put pour les users                             */
/* -------------------------------------------------------------------------- */
$app->put('/users/{id}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	// if (!isLogged()) {
	// 	return $resp->withStatus(401);   // Unauthorized
	// }
	$id = $args['id'];

	$params = $req->getParsedBody();
	$nom = $params['nom'];
	$prenom = $params['prenom'];
	$role = $params['idrole'];
	$entreprise = $params['identreprise'];
	$email = $params['email'];
	$mdp = $params['motdepasse'];
	if ($nom == '') {
		return $resp->withStatus(400);   // Bad request
	}

	$stmt = $pdo->prepare('SELECT * FROM `_user` WHERE email = :email AND WHERE NOT iduser = :id');
	$stmt->execute(['email' => $email, 'id' => $id]);

	if ($stmt->rowCount() == 0) {

		$stmt = $pdo->prepare("UPDATE `_user` SET `idrole` = :idrole, `identreprise` = :identreprise, `nom` = :nom, `prenom` = :prenom, `email` = :email, `motdepasse` = :motdepasse WHERE `iduser` = :id");
		$stmt->execute(['id' => $id, 'idrole' => $role, 'identreprise' => $entreprise, 'nom' => $nom, 'prenom' => $prenom, 'email' => $email, 'motdepasse' => $mdp]);

		return $resp->withStatus(200);
	} else {
		return $resp->withStatus(404);
	}
});

/* -------------------------------------------------------------------------- */
/*                         API Put pour les entreprises                       */
/* -------------------------------------------------------------------------- */
$app->put('/company/{id}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	// if (!isLogged()) {
	// 	return $resp->withStatus(401);   // Unauthorized
	// }
	$id = $args['id'];

	$params = $req->getParsedBody();
	$nom = $params['libele'];
	if ($nom == '') {
		return $resp->withStatus(400);   // Bad request
	}

	$stmt = $pdo->prepare('SELECT * FROM `_entreprise` WHERE libele = :nom WHERE NOT identreprise = :id');
	$stmt->execute(['id' => $id, 'nom' => $nom]);

	if ($stmt->rowCount() == 0) {

		$stmt = $pdo->prepare('UPDATE `_entreprise` SET `libele` = :nom WHERE identreprise = :id');
		$stmt->execute(['id' => $id, 'nom' => $nom]);

		return $resp->withStatus(200);
	} else {
		return $resp->withStatus(404);
	}
});

/* -------------------------------------------------------------------------- */
/*                         API Delete pour un document                        */
/* -------------------------------------------------------------------------- */
$app->get('/documents/delete/{id}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	$idParam = $args['id'];

	// if (!isLogged()&&$_SESSION['user_role']>2) {
	// 	return $resp->withStatus(401);   // Unauthorized
	// }

	$testIdDocument = $pdo->prepare('SELECT * FROM _document WHERE iddocument = ?');
	$testIdDocument->execute([$idParam]);
	$document = $testIdDocument->fetch();
	if (!$document) {
		return $resp->withStatus(404);  // Doesn't exist
	}

	$deleteStmnt = $pdo->prepare('DELETE FROM _document WHERE iddocument = ?');
	$deleteStmnt->execute([$idParam]);
	return $resp->withStatus(200);
});

// TODO VOIR AVEC ALEXANDRE
// ATTENTION Triger en bdd: En cas de suppression d'un user, ses droits sont supprimé.
// Si l'utilisateur en question est l'admin de l'entreprise, doit on aussi supprimé
// son entreprise (a noter que en cas de suppresion d'une entreprise leurs documents
// et utilisateurs sont supprimé par un triger) 
/* -------------------------------------------------------------------------- */
/*                           API Delete pour un user                          */
/* -------------------------------------------------------------------------- */
$app->delete('/users/{id}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	$idParam = $args['id'];

	// if (!isLogged()&&$_SESSION['user_role']>2) {
	// 	return $resp->withStatus(401);   // Unauthorized
	// }

	$testIdProduit = $pdo->prepare('SELECT * FROM _user WHERE iduser = ?');
	$testIdProduit->execute([$idParam]);
	$user = $testIdProduit->fetch();
	if (!$user) {
		return $resp->withStatus(404);  // Doesn't exist
	}
	if ($user['idrole'] === 2) {
		$deleteStmnt = $pdo->prepare('DELETE FROM _entreprise WHERE identreprise = ?');
		$deleteStmnt->execute([$user['identreprise']]);
	}

	$deleteStmnt = $pdo->prepare('DELETE FROM _user WHERE iduser = ?');
	$deleteStmnt->execute([$idParam]);
	return $resp->withStatus(200);
});


/* -------------------------------------------------------------------------- */
/*                         API Delete pour un tag                             */
/* -------------------------------------------------------------------------- */
$app->delete('/tags/{id}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	$idParam = $args['id'];

	// if (!isLogged()&&$_SESSION['user_role']>2) {
	// 	return $resp->withStatus(401);   // Unauthorized
	// }


	$testIdTag = $pdo->prepare('SELECT * FROM _tags WHERE idtags = ?');
	$testIdTag->execute([$idParam]);
	$tag = $testIdTag->fetch();
	if (!$tag) {
		return $resp->withStatus(404);  // Doesn't exist
	}

	$deleteStmnt = $pdo->prepare('DELETE FROM _tags WHERE idtags = ?');
	$deleteStmnt->execute([$idParam]);
	return $resp->withStatus(200);
});

/* -------------------------------------------------------------------------- */
/*                       API Delete pour un entrprise                         */
/* -------------------------------------------------------------------------- */
$app->delete('/company/{id}', function ($req, $resp, $args) {
	$pdo = initDatabase();
	$idParam = $args['id'];

	// if (!isLogged()&&$_SESSION['user_role']>2) {
	// 	return $resp->withStatus(401);   // Unauthorized
	// }

	$testIdEntreprise = $pdo->prepare('SELECT * FROM _entreprise WHERE identreprise = ?');
	$testIdEntreprise->execute([$idParam]);
	$entreprise = $testIdEntreprise->fetch();
	if (!$entreprise) {
		return $resp->withStatus(404);  // Doesn't exist
	}

	$deleteStmnt = $pdo->prepare('DELETE FROM _entreprise WHERE identreprise = ?');
	$deleteStmnt->execute([$idParam]);
	return $resp->withStatus(200);
});

// Fix "bug" (?) avec PUT vide (body non parsé)
$app->addBodyParsingMiddleware();
$app->run();
