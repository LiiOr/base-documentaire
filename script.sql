-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : mysql
-- Généré le : jeu. 31 mars 2022 à 08:35
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `info_doc`
--

-- --------------------------------------------------------

--
-- Structure de la table `_document`
--

CREATE TABLE `_document` (
  `iddocument` bigint(20) UNSIGNED NOT NULL,
  `idproprietaire` bigint(20) UNSIGNED NOT NULL,
  `nom` varchar(255) NOT NULL,
  `listeslug` varchar(255) NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `chemin` varchar(255) NOT NULL,
  `visibilite` tinyint(2) NOT NULL,
  `size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `_document`
--

INSERT INTO `_document` (`iddocument`, `idproprietaire`, `nom`, `listeslug`, `createdat`, `modifiedat`, `chemin`, `visibilite`, `size`) VALUES
(1, 2, 'imprimante', '1:2:3', '2022-01-31 15:39:46', '2022-01-31 15:39:46', 'ressources/facebook/imprimante', 0, 10000),
(2, 5, 'google doc', '4:2:3', '2022-01-31 15:39:46', '2022-01-31 15:39:46', '/ressources/google', 1, 10000000),
(3, 2, 'un Doc magique', '3:5', '2022-02-03 09:47:16', '2022-02-03 09:47:16', '/imprimante/manuel', 0, 1500),
(4, 7, 'Manuel XP3100', '3', '2022-02-03 09:47:16', '2022-02-03 09:47:16', '/HP/garantit', 1, 25000),
(5, 2, 'la doc un pepe', '3:2', '2022-02-03 09:49:49', '2022-02-03 09:49:49', '/Pepe/samuse', 1, 999999),
(16, 5, 'test', '2:3:4', '2022-02-03 10:40:34', '2022-02-03 10:40:34', 'test/marche/test', 1, 10000),
(17, 5, 'test', '2:3:4', '2022-02-03 10:42:48', '2022-02-03 10:42:48', 'wut?', 1, 10000),
(18, 5, 'test', '2:3:4', '2022-02-03 10:43:29', '2022-02-03 10:43:29', 'wut?', 1, 10000),
(19, 5, 'test', '2:3:4', '2022-02-03 10:43:30', '2022-02-03 10:43:30', 'wut?', 1, 10000),
(32, 9, 'lol2', '4', '2022-02-03 12:48:53', '2022-02-03 12:48:53', '/lol', 0, 10000),
(34, 2, 'blblbl', '1', '2022-02-07 15:40:52', '2022-02-07 15:40:52', 'blblbllblbl', 1, 5000),
(38, 2, 'test', '2:3:4', '2022-02-08 07:24:07', '2022-02-08 07:24:07', 'test/bidule/machin', 1, 10000),
(40, 2, 'test2', '2:3:4', '2022-02-08 07:31:01', '2022-02-08 07:31:01', 'test/marche/test', 1, 10000000),
(43, 2, 'aaaaaa', '1;2;3', '2022-02-11 10:23:11', '2022-02-11 10:23:11', '/chemin', 1, 10000),
(44, 2, 'aaaaaaaaa', '1;2;3', '2022-02-11 10:25:56', '2022-02-11 10:25:56', '/chemin', 1, 1000),
(50, 0, 'TestLaure2', '1', '2022-02-11 12:50:45', '2022-02-11 12:50:45', 'On verra ca apres', 1, 7696),
(52, 0, 'formulaire.zip', 'On verra ca après', '2022-02-11 13:45:31', '2022-02-11 13:45:31', 'On verra ca apres', 1, 0),
(83, 0, 'tp.tgz', 'On verra ca après', '2022-03-22 10:37:21', '2022-03-22 10:37:21', 'On verra ca apres', 1, 1112353),
(84, 0, 'test.zip', 'On verra ca après', '2022-03-22 10:48:47', '2022-03-22 10:48:47', 'On verra ca apres', 1, 7696),
(85, 0, 'test.zip', 'On verra ca après', '2022-03-22 10:50:19', '2022-03-22 10:50:19', 'On verra ca apres', 1, 7696),
(86, 0, 'test.zip', 'On verra ca après', '2022-03-22 10:58:10', '2022-03-22 10:58:10', 'On verra ca apres', 1, 7696),
(87, 0, 'test.zip', 'On verra ca après', '2022-03-22 10:59:58', '2022-03-22 10:59:58', 'On verra ca apres', 1, 7696),
(88, 0, 'test.zip', 'On verra ca après', '2022-03-22 11:12:30', '2022-03-22 11:12:30', 'On verra ca apres', 1, 7696);

-- --------------------------------------------------------

--
-- Structure de la table `_droit`
--

CREATE TABLE `_droit` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `iddroit` bigint(20) UNSIGNED NOT NULL,
  `identreprise` bigint(20) UNSIGNED NOT NULL,
  `iduser` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `_droit`
--

INSERT INTO `_droit` (`id`, `iddroit`, `identreprise`, `iduser`) VALUES
(1, 2, 5, 2),
(2, 2, 2, 3),
(3, 3, 5, 6),
(4, 3, 2, 7);

-- --------------------------------------------------------

--
-- Structure de la table `_entreprise`
--

CREATE TABLE `_entreprise` (
  `identreprise` bigint(20) UNSIGNED NOT NULL,
  `libele` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `_entreprise`
--

INSERT INTO `_entreprise` (`identreprise`, `libele`) VALUES
(0, 'info-doc'),
(2, 'google'),
(5, 'facebook'),
(6, 'ibm'),
(7, 'hp'),
(9, 'test2'),
(10, 'test');

--
-- Déclencheurs `_entreprise`
--
DELIMITER $$
CREATE TRIGGER `tr_entreprise_delete` BEFORE DELETE ON `_entreprise` FOR EACH ROW BEGIN
    DELETE FROM _user WHERE identreprise = OLD.identreprise;
    DELETE FROM _document WHERE idproprietaire = OLD.identreprise;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `_tags`
--

CREATE TABLE `_tags` (
  `idtags` bigint(20) UNSIGNED NOT NULL,
  `libele` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `_tags`
--

INSERT INTO `_tags` (`idtags`, `libele`) VALUES
(1, 'Materiel'),
(2, 'Papier'),
(3, 'Imprimante'),
(4, 'Domotique'),
(5, 'Info_doc_tuto'),
(7, 'test');

-- --------------------------------------------------------

--
-- Structure de la table `_typedroit`
--

CREATE TABLE `_typedroit` (
  `iddroit` bigint(20) UNSIGNED NOT NULL,
  `libele` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `_typedroit`
--

INSERT INTO `_typedroit` (`iddroit`, `libele`) VALUES
(1, 'superadmin'),
(2, 'admin'),
(3, 'utilisateur');

-- --------------------------------------------------------

--
-- Structure de la table `_typeuser`
--

CREATE TABLE `_typeuser` (
  `idrole` bigint(20) UNSIGNED NOT NULL,
  `libele` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `_typeuser`
--

INSERT INTO `_typeuser` (`idrole`, `libele`) VALUES
(1, 'superadmin'),
(2, 'admin'),
(3, 'utilisateur');

-- --------------------------------------------------------

--
-- Structure de la table `_user`
--

CREATE TABLE `_user` (
  `iduser` bigint(20) UNSIGNED NOT NULL,
  `idrole` bigint(20) UNSIGNED NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `motdepasse` varchar(255) NOT NULL,
  `identreprise` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `_user`
--

INSERT INTO `_user` (`iduser`, `idrole`, `nom`, `prenom`, `email`, `motdepasse`, `identreprise`) VALUES
(1, 1, 'admin', 'root', 'root@gmail.com', 'root', 0),
(2, 2, 'facebook', 'root', 'facebook@mail.com', 'facebook', 5),
(3, 2, 'google', 'root', 'google@mail.com', 'google', 2),
(4, 2, 'hp', 'root', 'hp@mail.com', 'hp', 7),
(5, 2, 'ibm', 'root', 'ibm@mail.com', 'ibm', 6),
(6, 3, 'user_f', 'user', 'userf@mail.com', 'userf', 5),
(7, 3, 'user_g', 'user', 'userg@mail.com', 'userg', 2),
(10, 2, 'admin1', 'root', 'admin1@mail.fr', 'lol', 9),
(11, 1, 'test', 'test', 'test@test', 'test', 0);

--
-- Déclencheurs `_user`
--
DELIMITER $$
CREATE TRIGGER `tr_user_delete` BEFORE DELETE ON `_user` FOR EACH ROW BEGIN
	DELETE FROM _droit WHERE iduser = OLD.iduser;
END
$$
DELIMITER ;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `_document`
--
ALTER TABLE `_document`
  ADD PRIMARY KEY (`iddocument`),
  ADD UNIQUE KEY `iddocument` (`iddocument`),
  ADD KEY `fk_document_idproprietaire` (`idproprietaire`);

--
-- Index pour la table `_droit`
--
ALTER TABLE `_droit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_droit_iddroit` (`iddroit`),
  ADD KEY `fk_droit_iduser` (`iduser`),
  ADD KEY `fk_droit_identreprise` (`identreprise`);

--
-- Index pour la table `_entreprise`
--
ALTER TABLE `_entreprise`
  ADD PRIMARY KEY (`identreprise`),
  ADD UNIQUE KEY `identreprise` (`identreprise`);

--
-- Index pour la table `_tags`
--
ALTER TABLE `_tags`
  ADD PRIMARY KEY (`idtags`),
  ADD UNIQUE KEY `idtags` (`idtags`);

--
-- Index pour la table `_typedroit`
--
ALTER TABLE `_typedroit`
  ADD PRIMARY KEY (`iddroit`),
  ADD UNIQUE KEY `iddroit` (`iddroit`);

--
-- Index pour la table `_typeuser`
--
ALTER TABLE `_typeuser`
  ADD PRIMARY KEY (`idrole`),
  ADD UNIQUE KEY `idrole` (`idrole`);

--
-- Index pour la table `_user`
--
ALTER TABLE `_user`
  ADD PRIMARY KEY (`iduser`),
  ADD UNIQUE KEY `iduser` (`iduser`),
  ADD KEY `fk_user_idrole` (`idrole`),
  ADD KEY `fk_user_identreprise` (`identreprise`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `_document`
--
ALTER TABLE `_document`
  MODIFY `iddocument` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT pour la table `_droit`
--
ALTER TABLE `_droit`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `_entreprise`
--
ALTER TABLE `_entreprise`
  MODIFY `identreprise` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `_tags`
--
ALTER TABLE `_tags`
  MODIFY `idtags` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `_typedroit`
--
ALTER TABLE `_typedroit`
  MODIFY `iddroit` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `_typeuser`
--
ALTER TABLE `_typeuser`
  MODIFY `idrole` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `_user`
--
ALTER TABLE `_user`
  MODIFY `iduser` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `_document`
--
ALTER TABLE `_document`
  ADD CONSTRAINT `fk_document_idproprietaire` FOREIGN KEY (`idproprietaire`) REFERENCES `_entreprise` (`identreprise`);

--
-- Contraintes pour la table `_droit`
--
ALTER TABLE `_droit`
  ADD CONSTRAINT `fk_droit_iddroit` FOREIGN KEY (`iddroit`) REFERENCES `_typedroit` (`iddroit`),
  ADD CONSTRAINT `fk_droit_identreprise` FOREIGN KEY (`identreprise`) REFERENCES `_entreprise` (`identreprise`),
  ADD CONSTRAINT `fk_droit_iduser` FOREIGN KEY (`iduser`) REFERENCES `_user` (`iduser`);

--
-- Contraintes pour la table `_user`
--
ALTER TABLE `_user`
  ADD CONSTRAINT `fk_user_identreprise` FOREIGN KEY (`identreprise`) REFERENCES `_entreprise` (`identreprise`),
  ADD CONSTRAINT `fk_user_idrole` FOREIGN KEY (`idrole`) REFERENCES `_typeuser` (`idrole`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;